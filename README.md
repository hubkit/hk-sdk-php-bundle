# hk-sdk-php-bundle

This bundle is a Symfony integration of hubkit/hk-sdk-php. It provide configurations and dependancy injection of necessary classes to use the SDK with the framework.

## Configuration

First, activate the bundle. Add this line inside `config/bundles.php` 

    Hubkit\SdkBundle\HubkitSdkBundle::class => ['all' => true],

To override basic configurations, you can specify it like :

```yaml
hubkit_sdk:
    api_endpoint: 'https://app.hubkit.cloud/api'
    api_version: 'v1'
    api_key: ''
```
## Usage

Use symfony service injection (service `hubkit_sdk.manager`) and then check the hubkit/hk-sdk-php to use it properly.

```php

use Hubkit\SdkBundle\HubkitSdk

public function __construct(HubkitSdk $hubkitSdk) {
   // ...
}
```
