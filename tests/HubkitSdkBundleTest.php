<?php

namespace Hubkit\SdkBundle\Tests;

use Hubkit\SdkBundle\HubkitSdkBundle;
use PHPUnit\Framework\TestCase;

/**
 * HubkitSdkBundleTest
 *
 * @uses TestCase
 */
class HubkitSdkBundleTest extends TestCase
{
    /**
     * testCanBeInstantiated
     */
    public function testCanBeInstantiated()
    {
        $bundle = new HubkitSdkBundle();

        $this->assertInstanceOf('Hubkit\SdkBundle\HubkitSdkBundle', $bundle);
    }
}
