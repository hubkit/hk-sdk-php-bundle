<?php

namespace Hubkit\SdkBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Hubkit\SdkBundle\DependencyInjection\HubkitSdkExtension;

/**
 * HubkitSdkExtensionTest
 *
 * @uses AbstractExtensionTestCase
 */
class HubkitSdkExtensionTest extends AbstractExtensionTestCase
{
    /**
     * testAfterLoadingServiceExist
     */
    public function testAfterLoadingServiceExist()
    {
        $this->load();

        $this->assertContainerBuilderHasService('hubkit_sdk.manager');
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall('hubkit_sdk.manager', 'setBaseUrl', ['https://app.hubkit.cloud/api/v1']);
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall('hubkit_sdk.manager', 'setApiKey', ['']);
    }

    /**
     * getContainerExtensions
     *
     * @return array
     */
    protected function getContainerExtensions()
    {
        return [new HubkitSdkExtension()];
    }
}
