<?php

namespace Hubkit\SdkBundle\Tests;

use PHPUnit\Framework\TestCase;
use Hubkit\SdkBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Processor;

/**
 * ConfigurationTest
 *
 * @uses TestCase
 */
class ConfigurationTest extends TestCase
{
    /**
     * testGetConfigTreeBuilder
     */
    public function testGetConfigTreeBuilder()
    {
        $configuration = new Configuration();

        $processor = new Processor();

        $expected = [
            'api_endpoint' => 'https://app.hubkit.cloud/api',
            'api_version' => 'v1',
            'api_key' => '',
        ];

        $actual = $processor->processConfiguration($configuration, []);

        $this->assertEquals($expected, $actual);
    }
}
