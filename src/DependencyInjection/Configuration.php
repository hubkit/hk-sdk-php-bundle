<?php

namespace Hubkit\SdkBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Configuration
 *
 * @uses ConfigurationInterface
 */
class Configuration implements ConfigurationInterface
{
    /**
     * getConfigTreeBuilder
     *
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('hubkit_sdk');

        $rootNode
            ->children()
                ->scalarNode('api_endpoint')
                    ->defaultValue('https://app.hubkit.cloud/api')
                    ->info('Endpoint URL where to call API')
                ->end()
                ->scalarNode('api_version')
                    ->defaultValue('v1')
                    ->info('Version of endpoint')
                ->end()
                ->scalarNode('api_key')
                    ->defaultValue('')
                    ->info('Api Key used for call methods')
                ->end()
            ->end()
            ;

        return $treeBuilder;
    }
}
