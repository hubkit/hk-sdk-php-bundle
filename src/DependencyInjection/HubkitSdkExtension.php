<?php

namespace Hubkit\SdkBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * HubkitSdkExtension
 *
 * @uses Extension
 */
class HubkitSdkExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $mergedConfig, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.yml');

        $this->loadConfig($mergedConfig, $container);

        $apiEndpoint = $container->getParameter('hubkit_sdk.api_endpoint');
        $apiVersion = $container->getParameter('hubkit_sdk.api_version');
        $apiKey = $container->getParameter('hubkit_sdk.api_key');

        $definition = $container->getDefinition('hubkit_sdk.manager');
        $definition->addMethodCall('setBaseUrl', [sprintf('%s/%s', $apiEndpoint, $apiVersion)]);
        $definition->addMethodCall('setApiKey', [$apiKey]);
    }

    /**
     * Loads configurations and add the configuration values to the application parameters.
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     *
     * @throws \InvalidArgumentException
     */
    private function loadConfig(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $processedConfiguration = $this->processConfiguration($configuration, $configs);

        foreach ($processedConfiguration as $key => $value) {
            $container->setParameter(
                $this->getAlias().'.'.$key,
                $value
            );
        }
    }
}
