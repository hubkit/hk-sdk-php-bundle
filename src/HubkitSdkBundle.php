<?php

namespace Hubkit\SdkBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * HubkitSdkBundle
 *
 * @uses Bundle
 */
class HubkitSdkBundle extends Bundle
{
}
